# ASP.NET Core rodando em Container Docker

O objetivo desse repositório é trazer nossa aplicação "**aspnetapp**" em .NET Core para dentro de um Container Docker.
Note que a aplicação "**aspnetapp**" é um exemplo, caso queria utilizar sua aplicação ... é necessário substituir todos os campos que contém o nome dessa aplicação;

### STACK:
* **.NET SDK** version 7.0.105
* **Docker Compose** version v2.17.2

### Requisitos para rodar o projeto: 

.NET SDK: para executar essa aplicação é necessário ter o SDK instalado, caso você não tenha: https://dotnet.microsoft.com/pt-br/download/dotnet?cid=getdotnetcorecli
Docker Compose: https://docs.docker.com/compose/install/other/

## Dockerfile

```
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /source

# copie csproj e restaure como camadas distintas
COPY *.sln .
COPY aspnetapp/*.csproj ./aspnetapp/
RUN dotnet restore ./aspnetapp

# copie todo o resto e crie o aplicativo
COPY aspnetapp/. ./aspnetapp/
WORKDIR /source/aspnetapp
RUN dotnet publish -c release -o /app --no-restore

# fase final/imagem
FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["dotnet", "aspnetapp.dll"]
```

Para buildar e criar uma imagem Docker execute o comando abaixo:

```
docker build . -t nome-da-sua-img:tag
```
### Explicando as flags:

* **"docker build"** -> comando para buildar a imagem do seu container docker
* **"."** -> o "." significa que iremos trazer tudo que está no diretório atual para dentro da imagem docker quando for "buildado".
* **"-t"** -> significa o versionamento da imagem "tag".

Detalhe: quando você iniciar o processo de build, a imagem docker trará a aplicação "aspnetapp" para dentro do container docker, então, caso você tenha um projeto seu em .NET, é só subistituir esse diretório no **Dockerfile**, na linha 7 e 11 pelo nome do diretório de sua aplicação.

para rodar a aplicação docker com o docker compose, basta rodar o comando abaixo:

```
docker compose up -d
```

Note que esse container está configurado para a porta: "**5062**"

Para descer o container:

```
docker compose down
```

Tutorial Completo: https://luks.dev.br/2023/04/22/conteinerizando-uma-aplicacao-asp-net-core-para-docker/#content