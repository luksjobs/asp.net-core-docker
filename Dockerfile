FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /source

# copie csproj e restaure como camadas distintas
COPY *.sln .
#projeto de exemplo "aspnetapp"
COPY aspnetapp/*.csproj ./aspnetapp/
#nessa etapa, será compilado o projeto "aspnetapp"
RUN dotnet restore ./aspnetapp 

# copie todo o resto e crie o aplicativo
COPY aspnetapp/. ./aspnetapp/
WORKDIR /source/aspnetapp
RUN dotnet publish -c release -o /app --no-restore

# fase final/imagem
FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["dotnet", "aspnetapp.dll"]